

* Install XWiki 11.2 + Jetty from the standard zip package
* Compile and deploy this project
* Create a page "Sandbox.Test" and attach 3 AnnotationClass objects to it
* Head to LoggingAdmin and enable debug logging for "myeventlistener"
* Delete page "Sandbox.Test"

Behaviour:
* When logging the context, an endless status.xml is produced in /tmp/ and CPU hits 100% while no such behaviour occurs when the logger for "myeventlistener" is not in debug level.
* When logging the event only: the logger logs individual object deletion as expected, however when a document containing objects gets deleted, the logger is called (the debugger stops at the logger call), but no log is produced.