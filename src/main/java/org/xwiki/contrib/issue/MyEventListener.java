package org.xwiki.contrib.issue;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.xwiki.component.annotation.Component;
import org.xwiki.observation.AbstractEventListener;
import org.xwiki.observation.event.Event;

import com.xpn.xwiki.internal.event.XObjectAddedEvent;
import com.xpn.xwiki.internal.event.XObjectDeletedEvent;
import com.xpn.xwiki.internal.event.XObjectUpdatedEvent;

@Component
@Named(MyEventListener.NAME)
@Singleton
public class MyEventListener extends AbstractEventListener
{
    public static final String NAME = "myeventlistener";

    @Inject
    protected Logger logger;

    public MyEventListener()
    {
        super(NAME, new XObjectAddedEvent(),
                new XObjectUpdatedEvent(),
                new XObjectDeletedEvent());
    }

    @Override
    public void onEvent(Event event, Object source, Object data)
    {

        logger.debug("Event: {} - Source: {} - Data: {}", event);
    }
}
